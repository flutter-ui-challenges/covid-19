import 'package:flutter/material.dart';

class TimeSinceFirstCaseTopClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.moveTo(0.0, 0.0);

    path.quadraticBezierTo(
      size.width * 1.4 / 12,
      5.0,
      size.width * 2.5 / 12,
      30,
    );

    path.quadraticBezierTo(
      size.width * 3.5 / 12,
      62.0,
      size.width * 5 / 12,
      48,
    );

    path.quadraticBezierTo(
      size.width * 7.2 / 12,
      30,
      size.width * 8.3 / 12,
      70,
    );

    path.quadraticBezierTo(
      size.width * 10.5 / 12,
      size.height - 30,
      size.width,
      size.height - 30.0,
    );
    path.lineTo(size.width, size.height);
    path.lineTo(0.0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
