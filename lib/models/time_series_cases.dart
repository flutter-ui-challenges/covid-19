class TimeSeriesCases {
  final DateTime time;
  final int cases;

  const TimeSeriesCases(this.time, this.cases);
}
