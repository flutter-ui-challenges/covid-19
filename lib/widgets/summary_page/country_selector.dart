import 'package:covid19/bloc/covid_statistics_bloc.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CountrySelector extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CovidStatisticsBloc, CovidStatisticsState>(
      builder: (ctx, state) {
        if (state is CovidStatisticsError ||
            state is CovidStatisticsUninitialized)
          return _buildOfflineCountrySelector();
        return _buildOnlineCountrySelector();
      },
    );
  }

  Widget _buildOfflineCountrySelector() => Container(
        height: 40.0,
        width: 150.0,
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40.0),
          color: Colors.white,
        ),
        child: Row(
          children: <Widget>[
            CircleAvatar(
              backgroundColor: Color(0xFFE62A3B),
            ),
            SizedBox(
              width: 8.0,
            ),
            Text(
              'OFFLINE',
              style: TextStyle(
                color: Color(0xFF032D44),
                fontSize: 14.0,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      );
  Widget _buildOnlineCountrySelector() {
    return GestureDetector(
      onTap: () {
        print("Selector de pais!");
      },
      child: Container(
        width: 200.0,
        height: 40.0,
        padding: const EdgeInsets.only(left: 4.0, right: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Row(
          children: <Widget>[
            CircleAvatar(
              backgroundColor: Color(0xFFEBEFF2),
              radius: 16.0,
              child: Icon(
                FeatherIcons.mapPin,
                color: Color(0xFF032D44),
                size: 16.0,
              ),
            ),
            SizedBox(
              width: 8.0,
            ),
            Expanded(
              child: Text(
                'Worldwide',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12.0,
                  color: Color(0xFF032D44),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
