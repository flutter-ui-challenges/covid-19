import 'package:covid19/widgets/summary_page/continents_pie_chart.dart';
import 'package:covid19/widgets/summary_page/top_chart.dart';
import 'package:flutter/material.dart';

import 'progress_chart.dart';
import 'time_since_first_case.dart';

class SummaryCharts extends StatelessWidget {
  const SummaryCharts({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TimeSinceFirstCase(),
        Container(
          padding: const EdgeInsets.all(16.0),
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Column(
            children: <Widget>[
              ProgressChart(),
              SizedBox(
                height: 16.0,
              ),
              ContinentsPieChart(),
              SizedBox(
                height: 16,
              ),
              TopChart(),
            ],
          ),
        ),
      ],
    );
  }
}
