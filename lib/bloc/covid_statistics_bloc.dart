import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:covid19/models/covid_statistics_model.dart';
import 'package:covid19/services/covid_statistics_service.dart';

part 'covid_statistics_event.dart';
part 'covid_statistics_state.dart';

class CovidStatisticsBloc
    extends Bloc<CovidStatisticsEvent, CovidStatisticsState> {
  final CovidStatisticsService covidService;

  CovidStatisticsBloc(this.covidService);
  @override
  CovidStatisticsState get initialState => CovidStatisticsUninitialized();

  @override
  void onTransition(
      Transition<CovidStatisticsEvent, CovidStatisticsState> transition) {
    print(transition);
    super.onTransition(transition);
  }

  @override
  Stream<CovidStatisticsState> mapEventToState(
    CovidStatisticsEvent event,
  ) async* {
    print('Consumiendo evento $event');
    if (event is FetchCovidStatisticsEvent) {
      try {
        if (event.countrySlug == 'worldwide') {
          yield CovidStatisticsLoaded(
            await covidService.fetchWorldwide(),
          );
        } else {
          yield CovidStatisticsLoaded(
            await covidService.fetchCountry(event.countrySlug),
          );
        }
      } catch (e) {
        print(e);
        yield CovidStatisticsError();
      }
    }
  }
}
