import 'package:covid19/widgets/app_header_clipper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppHeader extends StatelessWidget {
  final Color backgroundColor;

  const AppHeader({Key key, this.backgroundColor}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: AppHeaderClipper(),
      child: Container(
        height: 120.0,
        width: double.infinity,
        color: backgroundColor ?? Theme.of(context).appBarTheme.color,
        padding: const EdgeInsets.only(bottom: 24.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 2.0),
              child: SvgPicture.asset(
                'assets/images/CovidIcon.svg',
                height: 36.0,
                width: 36.0,
              ),
            ),
            SizedBox(
              width: 5.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  'CORONAVIRUS',
                  style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                Text(
                  'COVID-19',
                  style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                      color: Colors.white,
                      height: 1.0),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
