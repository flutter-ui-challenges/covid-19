import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';

class ContinentsPieChart extends StatelessWidget {
  final _colors = ["E6EAEC", "CDD5DA", "9AABB4", "68818F", "355769", "032D44"];
  @override
  Widget build(BuildContext context) {
    return PhysicalShape(
      elevation: 5,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      color: Colors.transparent,
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(32.0))),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'WORLDWIDE DISTRIBUTION',
              style: TextStyle(
                color: Color(0xFF032D44),
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [buildPieChart(), buildLegend()],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildLegend() => Column(
        children: <Widget>[
          buildLegendTile(0, 'Europa', 13000000),
          buildLegendTile(1, 'Europa', 14000000),
          buildLegendTile(2, 'Europa', 15000000),
          buildLegendTile(3, 'Europa', 16000000),
          buildLegendTile(4, 'Europa', 17000000),
        ],
      );

  Widget buildLegendTile(int index, String title, int value) => Container(
        margin: const EdgeInsets.symmetric(vertical: 4.0),
        child: Row(
          children: <Widget>[
            Container(
              width: 7.0,
              height: 30.0,
              decoration: BoxDecoration(
                color: Color(int.parse('0xFF${_colors[index]}')),
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
            SizedBox(
              width: 5.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(NumberFormat.compact().format(value)),
                Text(title)
              ],
            )
          ],
        ),
      );

  Widget buildPieChart() {
    return Container(
      height: 250,
      width: 250,
      padding: const EdgeInsets.only(top: 12.0),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          charts.PieChart(
            _createSampleData(),
            animate: false,

            // Configure the width of the pie slices to 60px. The remaining space in
            // the chart will be left as a hole in the center.
            defaultRenderer: new charts.ArcRendererConfig(arcWidth: 50),
          ),
          Text(
            '99.9 MM',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              color: Color(0xFF032D44),
            ),
          ),
        ],
      ),
    );
  }

  /// Create one series with sample hard coded data.
  List<charts.Series<LinearSales, int>> _createSampleData() {
    final data = [
      new LinearSales(0, 100),
      new LinearSales(1, 75),
      new LinearSales(2, 25),
      new LinearSales(3, 5),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        colorFn: (ls, i) => charts.Color.fromHex(code: '#${_colors[i]}'),
        data: data,
      )
    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
