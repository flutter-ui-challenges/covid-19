import 'package:covid19/bloc/covid_statistics_bloc.dart';
import 'package:covid19/models/covid_statistics_model.dart';
import 'package:covid19/widgets/summary_page/kpi_column_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class DiseaseSummary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CovidStatisticsBloc, CovidStatisticsState>(
      builder: (ctx, state) {
        CovidStatisticsModel stats = state.stats;
        return Stack(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                KpiColumnBar(
                  height: 200,
                  title: 'Countries',
                  subtitle: NumberFormat.compact()
                      .format(stats.affectedCountries)
                      .toString(),
                ),
                SizedBox(
                  width: 16.0,
                ),
                KpiColumnBar(
                  height: 216,
                  title: 'Deaths',
                  subtitle: NumberFormat.compact()
                      .format(stats.totalDeaths)
                      .toString(),
                ),
                SizedBox(
                  width: 16.0,
                ),
                KpiColumnBar(
                  height: 200,
                  title: 'Recovered',
                  subtitle: NumberFormat.compact()
                      .format(stats.totalRecovered)
                      .toString(),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
