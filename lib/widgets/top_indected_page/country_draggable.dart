import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CountryDraggable extends StatelessWidget {
  final Function onClose;

  CountryDraggable(this.onClose);
  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      minChildSize: 0.3,
      initialChildSize: 0.5,
      maxChildSize: 0.75,
      builder: (ctx, scrollController) {
        return SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: <Widget>[
              Container(
                height: 90,
                padding: const EdgeInsets.all(24.0),
                decoration: BoxDecoration(
                  color: Color(0xFFF4F6F9),
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(24.0),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          width: 60.0,
                          height: 40.0,
                          margin: const EdgeInsets.only(right: 8.0),
                          decoration: BoxDecoration(
                            color: Color(0xFFE6EAEC),
                            borderRadius: BorderRadius.circular(32.0),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            '1°',
                            style: TextStyle(
                              color: Color(0xFF032D44),
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                        Text(
                          'USA',
                          style: TextStyle(
                            color: Color(0xFF032D44),
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        )
                      ],
                    ),
                    IconButton(icon: Icon(FeatherIcons.x), onPressed: onClose),
                  ],
                ),
              ),
              _buildStats(),
              Container(
                  padding: const EdgeInsets.symmetric(vertical: 50.0),
                  color: Colors.white,
                  width: double.infinity,
                  child: Column(
                    children: <Widget>[
                      Icon(
                        FeatherIcons.cameraOff,
                        size: 50.0,
                      ),
                      SizedBox(
                        height: 100.0,
                      ),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: 'Sorry, but we do not have\nenough data from ',
                          style: TextStyle(
                            color: Color(0xFF032D44),
                            fontSize: 16.0,
                            height: 1.2,
                            letterSpacing: 0.3,
                          ),
                          children: [
                            TextSpan(
                              text: 'Chile\n',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            TextSpan(
                              text: 'to show',
                              style: TextStyle(
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )),
              // Container(
              //   color: Colors.white,
              //   child: ListView(
              //     primary: false,
              //     shrinkWrap: true,
              //     children: <Widget>[
              //       for (int i = 0; i < 20; i++) ListTile(title: Text('holis'))
              //     ],
              //   ),
              // ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildStats() {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 100.0,
              color: Color(0xFFF4F6F9),
            ),
            Container(
              width: double.infinity,
              height: 55.0,
              color: Colors.white,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _buidStatCard(169, 0),
            _buidStatCard(255000, 4990),
            _buidStatCard(1190000, 0),
          ],
        )
      ],
    );
  }

  Widget _buidStatCard(int value, int increase) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      elevation: 5.0,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        width: 105.0,
        height: 110.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Active'),
            Divider(
              height: 6.0,
              color: Color(0xFF032D44),
              thickness: 1.0,
            ),
            Text(NumberFormat.compact().format(value)),
            SizedBox(
              height: 4.0,
            ),
            if (increase > 0)
              Container(
                height: 15.0,
                width: 60.0,
                padding: const EdgeInsets.symmetric(
                  horizontal: 8.0,
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(16.0),
                ),
                child: Text(
                  '+${NumberFormat.compact().format(increase)}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 10.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
