import 'package:covid19/bloc/covid_statistics_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class KpiColumnBar extends StatelessWidget {
  final double height;
  final String title;
  final String subtitle;
  const KpiColumnBar({
    @required this.height,
    @required this.title,
    @required this.subtitle,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 16.0),
        child: Column(
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                color: Color(0xFF032D44),
                fontSize: 14.0,
                height: 1.2,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 3.0),
              height: 1.0,
              width: double.infinity,
              color: Color(0xFF032D44),
            ),
            BlocBuilder<CovidStatisticsBloc, CovidStatisticsState>(
                builder: (ctx, state) {
              if (state is CovidStatisticsUninitialized ||
                  state is CovidStatisticsError) return _buildOfflineText();
              return _buildOnlineText();
            })
          ],
        ),
      ),
    );
  }

  Widget _buildOfflineText() {
    return Container(
      margin: const EdgeInsets.only(top: 3.0),
      decoration: BoxDecoration(
        color: Color(0xFFF4F6F9),
        borderRadius: BorderRadius.circular(48.0),
      ),
      height: 25.0,
    );
  }

  Widget _buildOnlineText() {
    return FittedBox(
      fit: BoxFit.cover,
      child: Text(
        subtitle,
        style: TextStyle(
          color: Color(0xFF032D44),
          fontWeight: FontWeight.w600,
          fontSize: 20.0,
          height: 1.5,
        ),
      ),
    );
  }
}
