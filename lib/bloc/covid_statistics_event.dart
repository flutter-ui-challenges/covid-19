part of 'covid_statistics_bloc.dart';

@immutable
abstract class CovidStatisticsEvent {}

class FetchCovidStatisticsEvent extends CovidStatisticsEvent {
  final String countrySlug;

  FetchCovidStatisticsEvent(this.countrySlug);
}
