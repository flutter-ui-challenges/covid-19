import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';

class TopCountry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(
                height: 75,
              ),
              Container(
                width: 275.0,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(16.0),
                    bottomRight: Radius.circular(16.0),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          FeatherIcons.alertTriangle,
                          color: Colors.white,
                          size: 16.0,
                        ),
                        SizedBox(
                          width: 6.0,
                        ),
                        Text(
                          '219 K',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 12.0,
                            height: 1.5,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          FeatherIcons.check,
                          color: Colors.white,
                          size: 16.0,
                        ),
                        SizedBox(
                          width: 6.0,
                        ),
                        Text(
                          '219 K',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 12.0,
                            height: 1.5,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          FeatherIcons.plus,
                          color: Colors.white,
                          size: 16.0,
                        ),
                        SizedBox(
                          width: 6.0,
                        ),
                        Text(
                          '219 K',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 12.0,
                            height: 1.5,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: 75.0,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16.0),
              boxShadow: [
                BoxShadow(
                  offset: Offset(1.0, 2.0),
                  color: Color.fromARGB(64, 9, 35, 66),
                  spreadRadius: 0.0,
                  blurRadius: 4.0,
                ),
                BoxShadow(
                  offset: Offset(0.0, 10.0),
                  color: Color.fromARGB(32, 9, 35, 66),
                  spreadRadius: 4.0,
                  blurRadius: 4.0,
                ),
              ],
            ),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
              child: FittedBox(
                fit: BoxFit.contain,
                child: Text(
                  '1° USA',
                  style: TextStyle(
                    color: Color(0xFF355769),
                    fontWeight: FontWeight.w300,
                    fontSize: 46.0,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
