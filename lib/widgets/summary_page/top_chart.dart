import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';

class TopChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PhysicalShape(
      elevation: 5,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      color: Colors.transparent,
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(32.0))),
      child: Container(
        padding: const EdgeInsets.all(24.0),
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'TOP WORLDWIDE',
                  style: TextStyle(
                    color: Color(0xFF032D44),
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        CircleAvatar(
                          radius: 10.0,
                          backgroundColor: Theme.of(context).primaryColor,
                        ),
                        SizedBox(
                          width: 4.0,
                        ),
                        Text(
                          'Confirmed',
                          style: TextStyle(
                            color: Color(0xFF032D44),
                            fontSize: 9.0,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      width: 12.0,
                    ),
                    Row(
                      children: <Widget>[
                        CircleAvatar(
                          radius: 10.0,
                          backgroundColor: Color(0xFF19D5B7),
                        ),
                        SizedBox(
                          width: 4.0,
                        ),
                        Text(
                          'Deaths',
                          style: TextStyle(
                            color: Color(0xFF032D44),
                            fontSize: 9.0,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Container(
              width: double.infinity,
              height: 250,
              child: charts.BarChart(
                _createSampleData(context),
                animate: false,
                domainAxis: charts.OrdinalAxisSpec(
                  renderSpec: charts.SmallTickRendererSpec(
                    labelStyle: charts.TextStyleSpec(
                      fontFamily: 'Mali',
                      fontSize: 12,
                      lineHeight: 2,
                      color: charts.Color(
                        a: 0xFF,
                        r: 0x68,
                        g: 0x81,
                        b: 0x8F,
                      ),
                    ),
                    lineStyle: charts.LineStyleSpec(
                      thickness: 5,
                      color: charts.Color(
                        a: 0xFF,
                        r: 0xCD,
                        g: 0xD5,
                        b: 0xDA,
                      ),
                    ),
                  ),
                ),
                primaryMeasureAxis: charts.NumericAxisSpec(
                  tickFormatterSpec: charts.BasicNumericTickFormatterSpec(
                      (value) => NumberFormat.compact().format(value)),
                  renderSpec: charts.SmallTickRendererSpec(
                    labelStyle: charts.TextStyleSpec(
                      fontFamily: 'Mali',
                      fontSize: 12,
                      lineHeight: 2,
                      color: charts.Color(
                        a: 0xFF,
                        r: 0x68,
                        g: 0x81,
                        b: 0x8F,
                      ),
                    ),
                    lineStyle: charts.LineStyleSpec(
                      thickness: 5,
                      color: charts.Color(
                        a: 0xFF,
                        r: 0xCD,
                        g: 0xD5,
                        b: 0xDA,
                      ),
                    ),
                  ),
                ),
                defaultRenderer: new charts.BarRendererConfig(
                    // By default, bar renderer will draw rounded bars with a constant
                    // radius of 100.
                    // To not have any rounded corners, use [NoCornerStrategy]
                    // To change the radius of the bars, use [ConstCornerStrategy]
                    cornerStrategy: const charts.ConstCornerStrategy(4)),
                barGroupingType: charts.BarGroupingType.grouped,
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Create series list with multiple series
  static List<charts.Series<OrdinalSales, String>> _createSampleData(
      BuildContext context) {
    final desktopSalesData = [
      new OrdinalSales('USA', 1200000),
      new OrdinalSales('ESP', 600000),
      new OrdinalSales('ITA', 400000),
      new OrdinalSales('GBR', 425000),
      new OrdinalSales('RUS', 325000),
      new OrdinalSales('DEU', 275000),
      new OrdinalSales('BRA', 200000),
    ];

    final tableSalesData = [
      new OrdinalSales('USA', 300000),
      new OrdinalSales('ESP', 150000),
      new OrdinalSales('ITA', 500000),
      new OrdinalSales('GBR', 550000),
      new OrdinalSales('RUS', 50000),
      new OrdinalSales('DEU', 90000),
      new OrdinalSales('BRA', 150000),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Desktop',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: desktopSalesData,
        colorFn: (datum, index) =>
            charts.ColorUtil.fromDartColor(Theme.of(context).primaryColor),
      ),
      new charts.Series<OrdinalSales, String>(
        id: 'Tablet',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: tableSalesData,
        colorFn: (datum, index) =>
            charts.ColorUtil.fromDartColor(Color(0xFF19D5B7)),
      ),
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
