import 'package:flutter/material.dart';

const Color _primaryColor = Color(0xFF002F87);
const Color _accentColor = Color(0xFF0599B8);

ThemeData appTheme = ThemeData(
  scaffoldBackgroundColor: Color(0xFFF4F6F9),
  primaryColor: _primaryColor,
  accentColor: _accentColor,
  fontFamily: 'Muli',
  appBarTheme: ThemeData.light().appBarTheme.copyWith(
        color: _primaryColor,
      ),
);
