import 'package:covid19/widgets/summary_page/confirmed_cases.dart';
import 'package:covid19/widgets/summary_page/country_selector.dart';
import 'package:covid19/widgets/summary_page/disease_summary.dart';
import 'package:covid19/widgets/summary_page/summary_charts.dart';
import 'package:flutter/material.dart';

class SummaryPage extends StatelessWidget {
  final double appHeaderHeight = 120.0;
  final double spacer = 24.0;
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final offset = appHeaderHeight + spacer * 10.5;
    final screenHeight = mediaQuery.size.height - mediaQuery.viewPadding.top;
    final sheetMinSize = (screenHeight - offset) / screenHeight;
    print(sheetMinSize);
    return Stack(
      // alignment: Alignment.topCenter,
      children: <Widget>[
        Column(
          children: <Widget>[
            const SizedBox(
              height: 120.0,
            ),
            const SizedBox(height: 24.0),
            CountrySelector(),
            const SizedBox(height: 24.0),
            ConfirmedCases(),
            const SizedBox(height: 24.0),
            DiseaseSummary(),
          ],
        ),
        DraggableScrollableSheet(
          maxChildSize: 1.0,
          initialChildSize: sheetMinSize,
          minChildSize: sheetMinSize,
          expand: true,
          builder: (ctx, scrollController) =>
              NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
              return null;
            },
            child: ListView(
              controller: scrollController,
              children: [
                SummaryCharts(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
