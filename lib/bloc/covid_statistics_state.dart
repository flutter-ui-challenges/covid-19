part of 'covid_statistics_bloc.dart';

@immutable
abstract class CovidStatisticsState {
  final CovidStatisticsModel stats;

  CovidStatisticsState(this.stats);
}

class CovidStatisticsUninitialized extends CovidStatisticsState {
  CovidStatisticsUninitialized() : super(CovidStatisticsModel());
}

class CovidStatisticsError extends CovidStatisticsState {
  CovidStatisticsError() : super(CovidStatisticsModel());
}

class CovidStatisticsLoaded extends CovidStatisticsState {
  CovidStatisticsLoaded(CovidStatisticsModel stats) : super(stats);
}
