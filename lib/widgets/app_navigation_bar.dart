import 'package:flutter/material.dart';

class AppNavigationBar extends StatefulWidget {
  final int currentIndex;
  final List<AppNavigationBarItem> items;
  final Function(int) onTap;

  AppNavigationBar({this.currentIndex = 0, @required this.items, this.onTap});
  @override
  _AppNavigationBarState createState() => _AppNavigationBarState();
}

class _AppNavigationBarState extends State<AppNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            offset: Offset(0, 5),
            spreadRadius: 0.0,
            blurRadius: 10.0,
          ),
        ],
      ),
      width: double.infinity,
      height: 50,
      child: Wrap(
        spacing: 24.0,
        alignment: WrapAlignment.center,
        runAlignment: WrapAlignment.end,
        children: widget.items.map(
          (item) {
            return InkWell(
              radius: 5.0,
              onTap: () => widget.onTap(widget.items.indexOf(item)),
              child: AppNavigationBarItemWidget(
                iconData: item.iconData,
                enabled: widget.currentIndex == widget.items.indexOf(item),
              ),
            );
          },
        ).toList(),
      ),
    );
  }
}

class AppNavigationBarItem {
  final IconData iconData;
  AppNavigationBarItem({this.iconData});
}

class AppNavigationBarItemWidget extends StatelessWidget {
  final bool enabled;
  final IconData iconData;
  final Function onTap;

  AppNavigationBarItemWidget(
      {@required this.iconData, this.enabled, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Icon(
          iconData,
          color: Theme.of(context).primaryColor,
        ),
        const SizedBox(
          height: 12.0,
        ),
        Container(
          decoration: BoxDecoration(
            color:
                enabled ? Theme.of(context).primaryColor : Colors.transparent,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
            ),
          ),
          width: 64.0,
          height: 5.0,
        ),
      ],
    );
  }
}
