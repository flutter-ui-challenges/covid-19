import 'package:covid19/widgets/top_indected_page/country_draggable.dart';
import 'package:covid19/widgets/top_indected_page/country_tile.dart';
import 'package:covid19/widgets/top_indected_page/order_selector.dart';
import 'package:covid19/widgets/top_indected_page/top_country.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';

class TopInfectedPage extends StatefulWidget {
  @override
  _TopInfectedPageState createState() => _TopInfectedPageState();
}

class _TopInfectedPageState extends State<TopInfectedPage> {
  bool _draggableEnabled = false;
  final List<Widget> _initialWidgets = [
    const SizedBox(
      height: 120.0,
    ),
    OrderSelector(),
    const SizedBox(
      height: 24.0,
    ),
  ];

  void _onCountryTileSelected(int topIndex) {
    setState(() {
      _draggableEnabled = true;
    });
  }

  void _hideDraggable() => setState(() => _draggableEnabled = false);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ListView.builder(
          itemBuilder: (ctx, index) {
            if (index < _initialWidgets.length) return _initialWidgets[index];
            return GestureDetector(
              onTap: () =>
                  _onCountryTileSelected(index - _initialWidgets.length),
              child: buildCountryTile(index - _initialWidgets.length),
            );
          },
          itemCount: _initialWidgets.length + 10,
        ),
        ...buildDraggable(_draggableEnabled),
      ],
    );
  }

  Widget buildCountryTile(int index) {
    return index == 0 ? TopCountry() : CountryTile(position: index + 1);
  }

  List<Widget> buildDraggable(bool enabled) {
    return !enabled
        ? []
        : [
            Positioned.fill(
              child: GestureDetector(
                onTap: _hideDraggable,
                child: Container(
                  color: Colors.black45,
                ),
              ),
            ),
            CountryDraggable(_hideDraggable),
          ];
  }
}
