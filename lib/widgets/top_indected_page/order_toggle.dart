import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';

class OrderToggle extends StatelessWidget {
  final bool active;
  final IconData iconData;
  final String title;
  final Function onTap;
  const OrderToggle(
      {this.active = false, this.iconData, this.title, this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 40.0,
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
          color: active ? Colors.white : Colors.transparent,
          borderRadius: BorderRadius.circular(24.0),
        ),
        child: Row(
          children: <Widget>[
            if (active)
              CircleAvatar(
                radius: 14.0,
                backgroundColor: Color(0xFFEBEFF2),
                child: Icon(
                  FeatherIcons.arrowUp,
                  size: 16.0,
                  color: Colors.black,
                ),
              ),
            SizedBox(
              width: active ? 6.0 : 0.0,
            ),
            Row(
              children: <Widget>[
                Icon(iconData, size: 16.0),
                SizedBox(
                  width: 6.0,
                ),
                Text(title),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
