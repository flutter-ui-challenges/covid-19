import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';

class CountryTile extends StatelessWidget {
  final int position;
  final int confirmed;
  final int recovered;
  final int deaths;

  const CountryTile(
      {Key key, this.position, this.confirmed, this.recovered, this.deaths})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 24.0,
      ),
      child: Container(
        height: 90.0,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Row(
          children: <Widget>[
            Container(
              width: 60,
              height: double.infinity,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFFE6EAEC),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  bottomLeft: Radius.circular(8.0),
                ),
              ),
              child: Text(
                '$position°',
                style: TextStyle(
                  color: Color(0xFF032D44),
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(8.0),
                      bottomRight: Radius.circular(8.0),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12.0),
                        child: Text(
                          'Spain',
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Color(
                              0xFF032D44,
                            ),
                          ),
                        ),
                      ),
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            buildConterIndicator(FeatherIcons.alertTriangle, 1),
                            buildConterIndicator(FeatherIcons.check, 1),
                            buildConterIndicator(FeatherIcons.plus, 1)
                          ],
                        ),
                      )
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Row buildConterIndicator(IconData icon, int value) {
    return Row(
      children: <Widget>[
        Icon(
          icon,
          size: 16.0,
          color: Color(0xFF9AABB4),
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          '360.1K',
          style: TextStyle(
            color: Color(0xFF9AABB4),
          ),
        )
      ],
    );
  }
}
