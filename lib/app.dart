import 'package:covid19/app_theme.dart';
import 'package:covid19/bloc/covid_statistics_bloc.dart';
import 'package:covid19/screens/home_screen.dart';
import 'package:covid19/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CovidStatisticsBloc(ApiService())
        ..add(FetchCovidStatisticsEvent('worldwide')),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: appTheme,
        routes: {
          HomeScreen.routeName: (_) => HomeScreen(),
        },
      ),
    );
  }
}
