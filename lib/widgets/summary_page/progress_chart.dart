import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:covid19/bloc/covid_statistics_bloc.dart';
import 'package:covid19/models/time_series_cases.dart';

class ProgressChart extends StatelessWidget {
  static const recoveredColor = Color(0xFF19D5B7);
  List<charts.Series<TimeSeriesCases, DateTime>> _createSeries(
    BuildContext context,
    List<TimeSeriesCases> confirmed,
    List<TimeSeriesCases> recovered,
  ) {
    final primaryColor = Theme.of(context).primaryColor;
    return [
      charts.Series<TimeSeriesCases, DateTime>(
        id: 'Confirmed',
        domainFn: (cases, _) => cases.time,
        measureFn: (cases, _) => cases.cases,
        data: confirmed ?? [],
        strokeWidthPxFn: (_, i) => 5.0,
        colorFn: (_, i) => charts.Color(
          a: primaryColor.alpha,
          r: primaryColor.red,
          g: primaryColor.green,
          b: primaryColor.blue,
        ),
      )..setAttribute(
          charts.rendererIdKey,
          'cases_series',
        ),
      charts.Series<TimeSeriesCases, DateTime>(
        id: 'Recovered',
        domainFn: (cases, _) => cases.time,
        measureFn: (cases, _) => cases.cases,
        data: recovered ?? [],
        strokeWidthPxFn: (_, i) => 5.0,
        colorFn: (_, i) => charts.Color(
          a: recoveredColor.alpha,
          r: recoveredColor.red,
          g: recoveredColor.green,
          b: recoveredColor.blue,
        ),
      )..setAttribute(
          charts.rendererIdKey,
          'recovered_series',
        ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PhysicalShape(
      elevation: 5,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      color: Colors.transparent,
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(32.0))),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 18.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'DATA HISTORY',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          width: 16.0,
                          height: 16.0,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: Text(
                            'Confirmed',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 9.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      width: 12.0,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          width: 16.0,
                          height: 16.0,
                          decoration: BoxDecoration(
                            color: Color(0xFF69FF98),
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: Text(
                            'Recovered',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 9.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
            Container(
              height: 250,
              padding: const EdgeInsets.only(top: 12.0),
              child: BlocBuilder<CovidStatisticsBloc, CovidStatisticsState>(
                builder: (ctx, state) => charts.TimeSeriesChart(
                  _createSeries(
                    ctx,
                    state.stats.historicalConfirmed,
                    state.stats.historicalRecovered,
                  ),
                  animate: false,
                  customSeriesRenderers: [
                    charts.LineRendererConfig(
                      customRendererId: 'cases_series',
                      includeArea: true,
                      includePoints: true,
                      radiusPx: 4.0,
                      areaOpacity: 0.4,
                    ),
                    charts.LineRendererConfig(
                      customRendererId: 'recovered_series',
                      includeArea: true,
                      includePoints: true,
                      radiusPx: 4.0,
                      areaOpacity: 0.4,
                    )
                  ],
                  domainAxis: charts.DateTimeAxisSpec(
                    renderSpec: charts.SmallTickRendererSpec(
                      labelStyle: charts.TextStyleSpec(
                        fontFamily: 'Mali',
                        fontSize: 12,
                        lineHeight: 2,
                        color: charts.Color(
                          a: 0xFF,
                          r: 0x68,
                          g: 0x81,
                          b: 0x8F,
                        ),
                      ),
                      lineStyle: charts.LineStyleSpec(
                        thickness: 5,
                        color: charts.Color(
                          a: 0xFF,
                          r: 0xCD,
                          g: 0xD5,
                          b: 0xDA,
                        ),
                      ),
                    ),
                  ),
                  primaryMeasureAxis: charts.NumericAxisSpec(
                    tickFormatterSpec: charts.BasicNumericTickFormatterSpec(
                        (value) => NumberFormat.compact().format(value)),
                    renderSpec: charts.SmallTickRendererSpec(
                      labelStyle: charts.TextStyleSpec(
                        fontFamily: 'Mali',
                        fontSize: 12,
                        lineHeight: 2,
                        color: charts.Color(
                          a: 0xFF,
                          r: 0x68,
                          g: 0x81,
                          b: 0x8F,
                        ),
                      ),
                      lineStyle: charts.LineStyleSpec(
                        thickness: 5,
                        color: charts.Color(
                          a: 0xFF,
                          r: 0xCD,
                          g: 0xD5,
                          b: 0xDA,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
