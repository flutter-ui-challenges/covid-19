import 'package:covid19/bloc/covid_statistics_bloc.dart';
import 'package:covid19/widgets/summary_page/time_since_first_case_top_clipper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TimeSinceFirstCase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return PhysicalShape(
      clipBehavior: Clip.antiAlias,
      elevation: 24.0,
      color: Colors.white,
      clipper: TimeSinceFirstCaseTopClipper(),
      child: Container(
        width: double.infinity,
        height: 170.0,
        color: Colors.white,
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: <Widget>[
            Container(
              width: mediaQuery.size.width * 0.65,
              height: 70.0,
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(16.0),
                  bottomRight: Radius.circular(16.0),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 40.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'TIME SINCE FIRST CASE',
                      style: TextStyle(
                        color:
                            Theme.of(context).accentTextTheme.headline6.color,
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    BlocBuilder<CovidStatisticsBloc, CovidStatisticsState>(
                        builder: (ctx, state) {
                      if (state is CovidStatisticsUninitialized ||
                          state is CovidStatisticsError)
                        return Container(
                          width: 100.0,
                          height: 25.0,
                          decoration: BoxDecoration(
                            color: Colors.white60,
                            borderRadius: BorderRadius.circular(48.0),
                          ),
                        );
                      return Text(
                        '${DateTime.now().difference(state.stats.firstCase ?? DateTime.now()).inDays} DAYS',
                        style: TextStyle(
                          color:
                              Theme.of(context).accentTextTheme.headline6.color,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      );
                    }),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
