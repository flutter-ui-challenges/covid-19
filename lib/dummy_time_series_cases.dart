import 'package:charts_flutter/flutter.dart' as charts;
import 'package:covid19/models/time_series_cases.dart';

List<charts.Series<TimeSeriesCases, DateTime>> createSampleData() {
  final confirmedData = [
    TimeSeriesCases(DateTime(2020, 02, 10), 5000),
    TimeSeriesCases(DateTime(2020, 02, 20), 20000),
    TimeSeriesCases(DateTime(2020, 03, 09), 60000),
    TimeSeriesCases(DateTime(2020, 03, 15), 50000),
    TimeSeriesCases(DateTime(2020, 03, 20), 40000),
  ];
  final recoveredData = [
    TimeSeriesCases(DateTime(2020, 02, 10), 1000),
    TimeSeriesCases(DateTime(2020, 02, 20), 10000),
    TimeSeriesCases(DateTime(2020, 03, 09), 50000),
    TimeSeriesCases(DateTime(2020, 03, 15), 40000),
    TimeSeriesCases(DateTime(2020, 03, 20), 30000),
  ];
  return [
    charts.Series<TimeSeriesCases, DateTime>(
      id: 'Confirmed',
      domainFn: (cases, _) => cases.time,
      measureFn: (cases, _) => cases.cases,
      data: confirmedData,
      strokeWidthPxFn: (_, i) => 5.0,
      colorFn: (_, i) => charts.Color(
        a: 0xFF,
        r: 0xFE,
        g: 0xA9,
        b: 0x00,
      ),
    )..setAttribute(
        charts.rendererIdKey,
        'cases_series',
      ),
    charts.Series<TimeSeriesCases, DateTime>(
      id: 'Recovered',
      domainFn: (cases, _) => cases.time,
      measureFn: (cases, _) => cases.cases,
      data: recoveredData,
      strokeWidthPxFn: (_, i) => 5.0,
      colorFn: (_, i) => charts.Color(
        a: 0xFF,
        r: 0x69,
        g: 0xFF,
        b: 0x98,
      ),
    )..setAttribute(
        charts.rendererIdKey,
        'cases_series',
      ),
  ];
}
