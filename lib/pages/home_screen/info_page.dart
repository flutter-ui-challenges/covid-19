import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        SizedBox(
          height: 100.0,
        ),
        InfoTile(
          svgAsset: 'assets/images/whatis.svg',
          title: 'What is\nCOVID19?',
        ),
        InfoTile(
          svgAsset: 'assets/images/whatis-1.svg',
          title: 'Prevention',
        ),
        InfoTile(
          svgAsset: 'assets/images/whatis-2.svg',
          title: 'Symtopms',
        ),
        InfoTile(
          svgAsset: 'assets/images/whatis-3.svg',
          title: 'Treatment',
        ),
      ],
    );
  }
}

class InfoTile extends StatelessWidget {
  final String svgAsset;
  final String title;
  const InfoTile({
    Key key,
    this.svgAsset,
    this.title = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(24.0),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Container(
          width: double.infinity,
          height: 145.0,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Stack(
            alignment: Alignment.centerLeft,
            children: <Widget>[
              SvgPicture.asset(
                svgAsset,
                fit: BoxFit.fitHeight,
              ),
              Positioned(
                top: 50.0,
                left: 24.0,
                child: Icon(
                  FeatherIcons.arrowRight,
                  color: Color(0xFFCDD5DA),
                  size: 24.0,
                ),
              ),
              Positioned(
                top: 82.0,
                left: 24.0,
                child: Text(
                  title,
                  style: TextStyle(
                    color: Color(0xFF032D44),
                    fontSize: 20.0,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
