import 'package:covid19/bloc/covid_statistics_bloc.dart';
import 'package:covid19/models/covid_statistics_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ConfirmedCases extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          'Total Confirmed Cases',
          style: TextStyle(
            color: Color(0xFF68818F),
            fontSize: 14.0,
            height: 1.2,
          ),
        ),
        SizedBox(
          height: 4.0,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 66.0),
          child: BlocBuilder<CovidStatisticsBloc, CovidStatisticsState>(
            builder: (ctx, state) {
              if (state is CovidStatisticsLoaded) {
                CovidStatisticsModel stats = state.stats;

                return buildOnline(stats);
              }
              return buildOfflineContainer();
            },
          ),
        )
      ],
    );
  }

  Widget buildOnline(CovidStatisticsModel stats) {
    return Stack(
      alignment: Alignment.centerRight,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24.0),
          width: double.infinity,
          height: 70.0,
          decoration: BoxDecoration(
            color: Color.fromARGB((255 * 0.6).toInt(), 210, 218, 226),
            borderRadius: BorderRadius.circular(16.0),
          ),
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: FittedBox(
              fit: BoxFit.cover,
              child: Text(
                NumberFormat.compact().format(stats?.totalConfirmed ?? 0),
                style: TextStyle(
                  color: Color(0xFF355769),
                  fontSize: 38.0,
                ),
              ),
            ),
          ),
        ),
        CircleAvatar(
          radius: 24.0,
          backgroundColor: Colors.red,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.arrow_upward,
                  size: 10.0,
                  color: Colors.white,
                ),
                Text(
                  NumberFormat.compact().format(stats?.newConfirmed ?? 0),
                  style: TextStyle(
                    fontSize: 10.0,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

Widget buildOfflineContainer() {
  return Container(
    height: 75,
    width: 250,
    decoration: BoxDecoration(
      color: Color.fromARGB(128, 210, 218, 226),
      borderRadius: BorderRadius.circular(16.0),
    ),
    padding: const EdgeInsets.symmetric(
      horizontal: 36.0,
      vertical: 16.0,
    ),
    child: Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(48.0),
      ),
    ),
  );
}
