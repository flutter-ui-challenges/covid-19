import 'package:covid19/widgets/top_indected_page/order_toggle.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';

class OrderSelector extends StatefulWidget {
  @override
  _OrderSelectorState createState() => _OrderSelectorState();
}

class _OrderSelectorState extends State<OrderSelector> {
  var _selectedIndex = 0;

  void _onTap(int index) {
    if (_selectedIndex != index) setState(() => _selectedIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          OrderToggle(
            active: _selectedIndex == 0,
            iconData: FeatherIcons.alertTriangle,
            title: 'Confirmed',
            onTap: () => _onTap(0),
          ),
          OrderToggle(
            active: _selectedIndex == 1,
            iconData: FeatherIcons.check,
            title: 'Recovered',
            onTap: () => _onTap(1),
          ),
          OrderToggle(
            active: _selectedIndex == 2,
            iconData: FeatherIcons.plus,
            title: 'Deaths',
            onTap: () => _onTap(2),
          ),
        ],
      ),
    );
  }
}
