import 'time_series_cases.dart';

class CovidStatisticsModel {
  final int newConfirmed;
  final int totalConfirmed;
  final int newDeaths;
  final int totalDeaths;
  final int newRecovered;
  final int totalRecovered;
  final String country;
  final String countryCode;
  final String slug;
  final DateTime updated;
  final DateTime firstCase;
  final int affectedCountries;
  final List<TimeSeriesCases> historicalConfirmed;
  final List<TimeSeriesCases> historicalDeaths;
  final List<TimeSeriesCases> historicalRecovered;

  CovidStatisticsModel({
    this.country = '',
    this.countryCode = '',
    this.slug = '',
    this.newConfirmed = 0,
    this.totalConfirmed = 0,
    this.newDeaths = 0,
    this.totalDeaths = 0,
    this.newRecovered = 0,
    this.totalRecovered = 0,
    this.affectedCountries = 0,
    this.historicalConfirmed,
    this.historicalDeaths,
    this.historicalRecovered,
    this.firstCase,
    this.updated,
  });
}
