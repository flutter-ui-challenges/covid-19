import 'dart:convert';

import 'package:covid19/models/HttpException.dart';
import 'package:covid19/models/covid_statistics_model.dart';
import 'package:http/http.dart' as http;

import 'package:covid19/services/covid_statistics_service.dart';
import 'package:intl/intl.dart';

import '../models/time_series_cases.dart';

class ApiService implements CovidStatisticsService {
  final _baseUrl = 'https://corona.lmao.ninja/v22';

  @override
  Future<CovidStatisticsModel> fetchWorldwide() async {
    final response = await http.get('$_baseUrl/all').then((response) {
      if (response.statusCode >= 400) throw HttpException();
      return response;
    });
    final responseData = json.decode(response.body) as Map<String, dynamic>;
    //{"updated":1589239907886,
    ///"cases":4250574,
    ///"todayCases":72477,
    ///"deaths":286959,
    ///"todayDeaths":3225,
    ///"recovered":1525144,
    ///"active":2438471,
    ///"critical":46956,
    ///"casesPerOneMillion":545,
    ///"deathsPerOneMillion":36,
    ///"tests":48914703,
    ///"testsPerOneMillion":6271.7,
    ///"affectedCountries":214}
    final historical =
        await http.get('$_baseUrl/historical/all').then((response) {
      if (response.statusCode >= 400) throw HttpException();
      print(response.body);
      return response;
    });
    final historicalData = json.decode(historical.body) as Map<String, dynamic>;
    /*
    {
   "cases":{
      "4/12/20":1835145,
   },
   "deaths":{
      "4/12/20":119853,
   },
   "recovered":{
      "5/11/20":1423202
   }
}
    */
    return CovidStatisticsModel(
      newConfirmed: responseData['todayCases'],
      newDeaths: responseData['todayDeaths'],
      newRecovered: 0,
      totalConfirmed: responseData['cases'],
      totalDeaths: responseData['deaths'],
      totalRecovered: responseData['recovered'],
      affectedCountries: responseData['affectedCountries'],
      country: 'Worldwide',
      countryCode: '',
      slug: 'worldwide',
      firstCase: DateTime(
        2019,
        12,
        01,
      ), // Segun: https://en.wikipedia.org/wiki/COVID-19_pandemic_by_country_and_territory
      historicalConfirmed: Map<String, int>.from(historicalData['cases'])
          .entries
          .map(
            (entry) => TimeSeriesCases(
              DateFormat('M/d/yy').parse(
                entry.key,
              ),
              entry.value,
            ),
          )
          .toList(),
      historicalDeaths: Map<String, int>.from(historicalData['deaths'])
          .entries
          .map(
            (entry) => TimeSeriesCases(
              DateFormat('M/d/yy').parse(
                entry.key,
              ),
              entry.value,
            ),
          )
          .toList(),
      historicalRecovered: Map<String, int>.from(historicalData['recovered'])
          .entries
          .map(
            (entry) => TimeSeriesCases(
              DateFormat('M/d/yy').parse(
                entry.key,
              ),
              entry.value,
            ),
          )
          .toList(),
      updated: DateTime.fromMillisecondsSinceEpoch(
        responseData['updated'],
      ),
    );
  }

  @override
  Future<CovidStatisticsModel> fetchCountry(String countrySlug) async {}
}
