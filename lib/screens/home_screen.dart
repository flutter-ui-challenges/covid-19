import 'package:covid19/bloc/covid_statistics_bloc.dart';
import 'package:covid19/pages/home_screen/info_page.dart';
import 'package:covid19/pages/home_screen/rss_page.dart';
import 'package:covid19/pages/home_screen/summary_page.dart';
import 'package:covid19/pages/home_screen/top_infected_page.dart';
import 'package:covid19/widgets/app_header.dart';
import 'package:covid19/widgets/app_navigation_bar.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var _selectedIndex = 0;
  final _navBarItems = [
    AppNavigationBarItem(
      iconData: FeatherIcons.activity,
    ),
    AppNavigationBarItem(
      iconData: FeatherIcons.list,
    ),
    AppNavigationBarItem(
      iconData: FeatherIcons.info,
    ),
    AppNavigationBarItem(
      iconData: FeatherIcons.rss,
    ),
  ];
  // bool _isInit = false;

  @override
  void didChangeDependencies() {
    // Provider.of<Countries>(context, listen: false).fetchSummary();
    super.didChangeDependencies();
  }

  void _onNavigationBarTapped(index) {
    if (_selectedIndex != index)
      setState(() {
        _selectedIndex = index;
      });
  }

  @override
  Widget build(BuildContext context) {
    final List<Map<String, Object>> _pages = [
      {
        'widget': SummaryPage(),
        'appBarColor': Theme.of(context).accentColor,
      },
      {
        'widget': TopInfectedPage(),
        'appBarColor': Color(0xFF014491),
      },
      {
        'widget': InfoPage(),
        'appBarColor': Color(0xFF19D5B7),
      },
      {
        'widget': RssPage(),
        'appBarColor': Theme.of(context).accentColor,
      }
    ];
    return Scaffold(
      body: BlocListener<CovidStatisticsBloc, CovidStatisticsState>(
        listener: (ctx, state) {
          if (state is CovidStatisticsError) {
            Scaffold.of(ctx).hideCurrentSnackBar();
            Scaffold.of(ctx).showSnackBar(SnackBar(
              content: Text('Communication error'),
              duration: Duration(days: 365),
              behavior: SnackBarBehavior.fixed,
              action: SnackBarAction(
                label: "CLOSE",
                onPressed: () {},
              ),
            ));
          }
        },
        child: Stack(
          children: <Widget>[
            Image.asset('assets/images/CovidBG.png'),
            _pages[_selectedIndex]['widget'] as Widget,
            AppHeader(backgroundColor: _pages[_selectedIndex]['appBarColor']),
          ],
        ),
      ),
      bottomNavigationBar: AppNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _onNavigationBarTapped,
        items: _navBarItems,
      ),
    );
  }
}
