import 'package:covid19/models/covid_statistics_model.dart';

abstract class CovidStatisticsService {
  Future<CovidStatisticsModel> fetchWorldwide();

  Future<CovidStatisticsModel> fetchCountry(String countrySlug);
}
